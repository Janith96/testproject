/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { Navigator, StatusBar, Platform, StyleSheet, View } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';



import LoginScreen from './src/components/loginScreen/LoginScreen';
import CustomerScreen from './src/components/customerScreen/CustomerScreen';
import CustomerDetail from './src/components/customerScreen/CustomerDetail';
// import CustomerScreen from './src/components/customerScreen/CustomerScreenOne12';
import SaveCust from './src/components/customerScreen/SaveCust';
import Test from './src/components/customerScreen/Test';


const RootStack = createStackNavigator({


  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: { header: null }

  },

  CustomerScreen: {
    screen: CustomerScreen,
    navigationOptions: { header: null }

  },

  CustomerDetail:{
    screen:CustomerDetail,
    navigationOptions:{header:null}
  },
  
  SaveCust: {
    screen:SaveCust,
    navigationOptions:{header:null}
  },
  
  Test:{
    screen:Test,
    navigationOptions:{header:null}
  },
},

  {
    initialRouteName: 'LoginScreen',


  },
  {
    headerMode: 'screen'
  },
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />
      </View>
    );
  }
}




//----------------------------------------------------------------------------//

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


