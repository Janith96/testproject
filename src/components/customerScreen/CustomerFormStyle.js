import { StyleSheet, PixelRatio } from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import Colors from '../../resources/Colors'
export default StyleSheet.create({

    mainView: {
        height: hp('100%'),
        width: wp('100%')
    },

    button: {
        backgroundColor: '#126F85',
        marginTop: 350,
        justifyContent: "center",
        left: 200,
        borderTopLeftRadius: 35,
        color:'#FFFF',
        height: 60,
        width: 300,
        shadowColor: "#000",
        // shadowOpacity: 0.99,
        // shadowRadius: 15.93,
        elevation: 21,
    },

    txtName: {
        position: 'absolute',
        top: 230,
        left: 30,
        height: 50,
        width: 300,
        backgroundColor: '#FFFF',
        borderRadius: 50,
        paddingLeft: 30,
        fontSize: 18,
        color:'#168EB4',
        shadowColor: "#000",
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 10,
    },

    imgUser: {
        width: 20,
        height: 20,
        right: 10,

    },

    txtAddress: {
        position: 'absolute',
        top: 300,
        left: 30,
        height: 50,
        width: 300,
        backgroundColor: '#FFFF',
        borderRadius: 50,
        paddingLeft: 30,
        shadowColor: '#000',
        color:'#168EB4',
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 10,
    },

    imgAddress: {
        width: 20,
        height: 20,
        right: 10,

    },

    txtContect: {
        position: 'absolute',
        top: 370,
        left: 30,
        height: 50,
        width: 300,
        backgroundColor: '#FFFF',
        borderRadius: 50,
        paddingLeft: 30,
        color:'#168EB4',
        shadowColor: "#000",
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 10,
    },
    
    imgContect: {
        width: 20,
        height: 20,
        right: 10,

    },

    txtSalary: {
        position: 'absolute',
        top: 440,
        left: 30,
        height: 50,
        width: 300,
        backgroundColor: '#FFFF',
        borderRadius: 50,
        paddingLeft: 30,
        color:'#168EB4',
        shadowColor: "#000",
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 10,
    },

    imgNic: {
        width: 20,
        height: 20,
        right: 10,
    },

    header: {
        color: "#FFFF",
        fontSize: 23,
        marginTop: 25,
        right:50,
    },

    formtag: {
        width: '100%',
        height: '100%',
        marginTop: 42,
        overflow: 'hidden',
    },

    headerOne: {
        backgroundColor: '#126F85',
        height: 80,
        borderBottomRightRadius: 70,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.53,
        shadowRadius: 15.98,
        elevation: 21,
    },
    Imagepicker: {
        borderRadius: 100,
        borderWidth: 2,
        borderColor: "#FFFF",
        width: 100,
        height: 100,
        bottom: 0,
        right: 40,
    },
    ImagepickerOne: {
        width: 50,
        height: 50,
        left: 22,
        top: 25
    },
    upImg: {
        borderRadius: 100,
        width: 100,
        height: 100,
        bottom: 52,
        right: 2,
    },
    itemLabel: {
        width: 200
    },
    bodyView: {
        backgroundColor: '#FFFF',
        borderRadius: 30,
        overflow: "hidden",
        width: '88%',
        height: '78%',
        position: "relative",
        left: 30,
        top: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.53,
        shadowRadius: 15.98,
        elevation: 21,
    },
    bodyBoxView: {
        height: '30%',
        width: '70%',
        backgroundColor: '#126F85',
        top: 20,
        left: 125,
        margin: 0,
        borderBottomLeftRadius: 40,
        borderTopLeftRadius: 40,
        justifyContent: "center",
        alignItems: "center",

    },
    textButton: {
        height: 60,
        width: 300,
        color: '#FFFF',
        fontSize: 19,
        top: 15,
        left: 50
    }

});
