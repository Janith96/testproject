import React from 'react';
import { StyleSheet, View, Text, AsyncStorage, Button } from 'react-native';

export default class SaveCust extends React.Component {
  state = {
    name: '',
    address: '',
    contect: '',
    nic: ''


  }
  
  componentWillMount() {
    this.displayData();
  }

  displayData = async () => {
    console.log("SaveCust")
    try {
      let user = await AsyncStorage.getItem('user');
      let parsed = JSON.parse(user);
      console.log(JSON.stringify(parsed));
      // console.log("working SaveCust"+JSON.stringify(parsed.name))
      // this.props.navigation.navigate('Test')

      this.setState({
        name: parsed.name,
        address: parsed.address,
        contect: parsed.contect,
        nic: parsed.nic
      })

    }
    catch (error) {
      alert(error)
    }
  }


  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>

        <Text style={styles.textStyle}>User Name: {this.state.name}</Text>
        <Text style={styles.textStyle}>User Address: {this.state.address}</Text>
        <Text style={styles.textStyle}>User Contect: {this.state.contect}</Text>
        <Text style={styles.textStyle}>User NIC: {this.state.nic}</Text>
        <View style={styles.buttonStyle}>
          <Button
            title="Go back"
            onPress={() => this.props.navigation.goBack()}
          />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 23,
    textAlign: 'center',
    color: '#f00',
  },

  buttonStyle: {
    width: "93%",
    marginTop: 50,
    backgroundColor: "red",
  }
});   