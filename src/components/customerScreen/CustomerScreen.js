
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, Alert, TouchableOpacity, StatusBar, TextInput,
    AsyncStorage
} from 'react-native';
import {
    Header, Left, Title, Right, Container, Content, Item, Input, Label,
    Button, Card, Form
} from 'native-base';
// import { showMessage } from 'react-native-flash-message';
import ImagePicker from 'react-native-image-picker';
// import { createStackNavigator, createAppContainer } from 'react-navigation';  
import styles from './CustomerFormStyle';
// import { ScrollView } from 'react-native-gesture-handler';
import CompressImage from 'react-native-compress-image';
import { ScrollView } from 'react-native-gesture-handler';
import base64 from 'react-native-base64'
// import Spinner from 'react-native-loading-spinner-overlay';



const options = {
    title: "Select Image",
    takePhotoButtonTitle: "From Camera",
    chooseFromLibraryButtonTitle: "From Gallery"
};

export default class CustomerForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            userAddress: '',
            userContect: '',
            userNic: '',
            avatarSource: null,
            image: null,
            url: '',
            loading: false,
            password: '',
            data: [],
            loading: false,
        };
    }



    // saveDataToApi() {
    //     var data = new FormData();
    //     data.append("username", "ABCD");
    //     fetch('http://5.189.148.181:8080/sales-drive/api/clientDetail', {
    //         method: 'POST',
    //         headers: {
    //             Accept: 'application/json',
    //             'Content-Type': 'multipart/form-data',
    //             'Authorization': this.state.token
    //         },
    //     })
    //         .then((response) => response.json())
    //         .then((responseJson) => {
    //             console.log('response object:', responseJson)
    //         })
    //         .catch((error) => {
    //             console.error(error);
    //         });
    // }

    // // save to data passing
    // saveData() {
    //     let obj = {
    //         name: this.state.userName,
    //         address: this.state.userAddress,
    //         contect: this.state.userContect,
    //         nic: this.state.userNic
    //     }
    //     AsyncStorage.setItem('user', JSON.stringify(obj));
    //     this.props.navigation.navigate('SaveCust')
    //     console.log("working customerScreen" + JSON.stringify(obj))
    // }


    state = {
        //default loading false
        loading: false,
    };


    //To compress a image
    compress() {
        CompressImage.createCompressedImage(this.state.image.uri, "Compress/Images")

            .then(response => {
                this.state.image.uri = response.uri;
                this.state.image.path = response.path;
                this.state.image.mime = response.mime;
            })

            .catch(err => {
                return Alert.alert(
                    Strings.COMPRESS_ERR
                );
            });

    }

    //To upload a image
    myPhoto = () => {

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            this.setState({

                image: {
                    uri: response.path,
                    width: response.width,
                    height: response.height
                }
            });
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('Image Picker Error: ', response.error);
            }

            else {
                let source = { uri: response.uri };
                this.compress();
                this.setState({
                    avatarSource: source
                });
            }
        });
    }
    //To upload a image
    // myfun = () => {
    //     // ImagePicker.showImagePicker(options, response => {
    //     ImagePicker.showImagePicker(options, (response) => {
    //         this.setState({

    //             image: {
    //                 uri: response.path,
    //                 width: response.width,
    //                 height: response.height
    //             }
    //         });

    //         if (response.didCancel) {
    //         } else if (response.error) {

    //         } else {

    //             let source = { uri: response.uri };
    //             this.compress();
    //             this.setState({
    //                 avatarSource: source
    //             });
    //         }
    //     });

    // };



    showAlert() {
        Alert.alert(this.state.username + "  " + this.state.userAddress + "  " +
            this.state.userContect + "    " + this.state.userNic)
    }

    //Mobile Validate
    mobilevalidate(text) {
        const reg = /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|5|6|7|8)\d)\d{6}$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    //NIC validator
    nicvalidatate(text) {
        const reg = /^([0-9]{9}[x|X|v|V]|[0-9]{12})$/
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    namevalidate(text) {
        const reg = /^[a-zA-Z ]*$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }

    validation() {

        if (this.namevalidate(this.state.name) && this.namevalidate(this.state.userAddress) &&
            this.mobilevalidate(this.state.userContect) && this.nicvalidatate(this.state.userNic)) {
            // this.props.navigation.navigate('CustomerDetail', {
            //     username: this.state.userName, address: this.state.userAddress,
            //     contect: this.state.userContect, nic: this.state.userNic
            // })         
            // this.saveCustomer();
        } else {
            Alert.alert("username or address, contect, nic incorrect!")
        }
    }

    // if (this.state.userName == "" && this.state.Password == "" && this.state.Password.length > 5) {
    //     this.props.navigation.navigate('CustomerScreen')
    // } 

    // saveCustomer() {

    //     var data = JSON.stringify({
    // businessName: this.state.userName,
    // address: this.state.userAddress,
    // mobile: this.state.userContect,
    // email: this.state.userNic

    //         // businessName: this.state.username,
    //         // businessType: "",
    //         // owner: "",
    //         // address: this.state.userAddress,
    //         // mobile: this.state.userContect,
    //         // email: this.state.userNic,
    //         // longitude:0 ,
    //         // latitude: 0,
    //         // firstImagePath: "",
    //         // secondImagePath: "",
    //         // thridImagePath: "",
    //         businessName: "nethadun",
    //         businessType: "",
    //         owner: "",
    //         address: "galle",
    //         mobile: "0772517571",
    //         email: "992609559v",
    //         longitude: 0,
    //         latitude: 0,
    //         firstImagePath: "",
    //         secondImagePath: "",
    //         thridImagePath: ""
    //     })
    //     // For Sending items
    //     fetch('http://5.189.148.181:8080/sales-drive/api/clientDetail', {
    //         method: 'POST',
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             // 'Authorization': 'Basic' + base64.encode(this.state.userName + " : " + this.state.password)
    //         },

    //         body: JSON.stringify(
    //             data

    //             // menu_price: this.state.price,
    //         )
    //     }).then(res => res.json())
    //         .then((responseJson) => {
    //             Alert.alert(JSON.stringify(responseJson))
    //             console.log(responseJson);
    //         })
    //         console.log(data+"Hellow");
    //     // .catch(error => console.error('Error:', error))
    // }

    saveCustomer() {
        this.setState({
            loading: true
        })
        var data = JSON.stringify({
            businessName: this.state.username,
            address: this.state.userAddress,
            mobile: this.state.userContect,
            email: this.state.userNic
            // imgPath: this.state.url

        })



        fetch('http://5.189.148.181:8080/sales-drive/api/clientDetail', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: data
        })
            .then((resp) => resp.json())
            .then((responseJson) => {

                console.log("response :" + JSON.stringify(responseJson))
                this.clearText();
            })
            .catch((error) => {
                this.setState({
                    loading: false
                })
                console.error(error + data);
            });

    }

    render() {
        return (
            <ScrollView  >
                <View style={styles.mainView}>
                    <Header style={styles.headerOne} >
                        <Text style={styles.header}>Customer Form</Text>
                    </Header>
                    <StatusBar
                        barStyle='dark-content'
                        backgroundColor='#126F85'
                    />
                    <View style={styles.bodyView}>
                        <View style={styles.bodyBoxView}>
                            <TouchableOpacity style={styles.Imagepicker}
                                onPress={this.myPhoto}>
                                <Image style={styles.ImagepickerOne} source={require('../../assets/camera.png')} />
                                <Image source={this.state.avatarSource} style={styles.upImg}></Image>

                            </TouchableOpacity>
                        </View>
                        <Item style={styles.txtName}>
                            <Image style={styles.imgUser} source={require('../../assets/userName.png')} />
                            <TextInput
                                placeholder='Username'
                                placeholderTextColor='black'
                                onChangeText={text => this.setState({ username: text })}
                                value={this.state.username}
                            >
                            </TextInput>
                        </Item>
                        <Item style={styles.txtAddress}>
                            <Image style={styles.imgUser} source={require('../../assets/location.png')} />
                            <TextInput
                                placeholder='Useraddress'
                                placeholderTextColor='black'
                                onChangeText={text => this.setState({ userAddress: text })}
                                value={this.state.userAddress}
                            >
                            </TextInput>
                        </Item>
                        <Item style={styles.txtContect}>
                            <Image style={styles.imgUser} source={require('../../assets/contact.png')} />
                            <TextInput
                                placeholder='Usercontect'
                                placeholderTextColor='black'
                                onChangeText={text => this.setState({ userContect: text })}
                                value={this.state.userContect}
                                keyboardType="numeric"
                            >
                            </TextInput>
                        </Item>
                        <Item style={styles.txtSalary}>
                            <Image style={styles.imgUser} source={require('../../assets/id.png')} />
                            <TextInput
                                placeholder='Usernic'
                                placeholderTextColor='black'
                                onChangeText={text => this.setState({ userNic: text })}
                                value={this.state.userNic}
                            >
                            </TextInput>
                        </Item>
                        <Button style={styles.button}
                            onPress={() => this.saveCustomer()}>
                            <Text style={styles.textButton}>
                                Save        </Text>
                        </Button>
                    </View >
                </View>
            </ScrollView >
        );
    }
}


