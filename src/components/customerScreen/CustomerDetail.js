import React from 'react';
import { StyleSheet, View, Text, AsyncStorage, Button, Image, FlatList } from 'react-native';
import base64 from 'react-native-base64';
export default class CustomerDetail extends React.Component {

    state = {
        name: '',
        address: '',
        contect: '',
        nic: '',
        userName: 'Vidura',
        password: '1234',
        data: []
    }

    componentDidMount() {
        // // this.displayData();
        // this.setState({
        //     name: this.props.navigation.state.params.username,
        //     address: this.props.navigation.state.params.address,
        //     contect: this.props.navigation.state.params.contect,
        //     nic: this.props.navigation.state.params.nic
        // })
        this.getAllCustomer()
    }



    getAllCustomer() {
        fetch("http://5.189.148.181:8080/sales-drive/api/clientDetail?text=&count=20&page=0 ",
            {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    'Authorization': 'Basic ' + base64.encode(this.state.userName + ":" + this.state.password),
                }
            }
        )
            .then(response => {
                console.log(JSON.stringify(response))
                return response.json()
            })
            .then(responseJson => {
                console.log(JSON.stringify(responseJson))
                this.setState({
                    data: responseJson
                })

            })
            .catch((error) => {
                console.log(error);



            });
    }

    

    render() {
        return (
            <View style={{
                flex: 1, alignItems: "center", justifyContent: "center",
                backgroundColor: '#D8D8D8'
            }}>

                {/* <Text style={styles.textStyle}>User Name: {this.state.name}</Text>
                <Text style={styles.textStyle}>User Address: {this.state.address}</Text>
                <Text style={styles.textStyle}>User Contect: {this.state.contect}</Text>
                <Text style={styles.textStyle}>User NIC: {this.state.nic}</Text>
                <View style={styles.buttonStyle}>
                    <Button
                        title="Go back"
                        onPress={() => this.props.navigation.goBack()}
                    />
                </View> */}
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <View style={styles.itemView}>
                            <Text style={styles.item} numberOfLines={1}>{item.address}</Text>
                            <Text style={styles.item}>{item.id}</Text>
                            {/* <Text style={styles.item}>{item.mobile}</Text> */}
                            <Image style={styles.itemImage} source={{
                                uri: 'http://5.189.148.181:8080/sales-drive/'
                                    + item.firstImagePath
                            }}></Image>
                        </View>
                    }
                />

            </View>
        );
    }
}
const styles = StyleSheet.create({
    textStyle: {
        fontSize: 23,
        textAlign: 'center',
        color: '#f00',
    },

    // buttonStyle: {
    //     width: "93%",
    //     marginTop: 50,
    //     backgroundColor: "red",
    // },
    itemView: {
        borderRadius: 30,
        paddingLeft: 30,
        backgroundColor: '#FFFF',
        height: 70,
        width: 350,
        margin: 7,
        color: "#168EB4",
        fontSize: 30,
        shadowColor: "#000",
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 21,

    },
    itemImage: {
        width: 60,
        height: 60,
        borderRadius: 50,
        bottom: 32,
        left: 255,
        // shadowColor: "#000",
        // shadowOpacity: 0.60,
        // shadowRadius: 15.93,
        // elevation: 15,
    },
    imageViewMain:{
        width: 60,
        height: 60,
        borderRadius: 50,
        bottom: 32,
        left: 255,
        // shadowColor: "#000",
        // shadowOpacity: 0.60,
        // shadowRadius: 15.93,
        // elevation: 15,
    },
}); 