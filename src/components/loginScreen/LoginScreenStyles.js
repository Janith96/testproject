import { StyleSheet, PixelRatio } from 'react-native'
// import Colors from '../../resources/Colors'
export default StyleSheet.create({

    imageBackground: {
        width: '150%',
        height: '100%'
    },
    bodyView: {
        backgroundColor: '#FFFF',
        borderRadius: 30,
        overflow: "hidden",
        width: '62%',
        height: '85%',
        position: "relative",
        marginTop: 50,
        left: 15,
        // justifyContent: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.53,
        shadowRadius: 15.98,
        elevation: 21,
    },
    bodyBoxView: {
        height: '30%',
        width: '70%',
        backgroundColor: '#126F85',
        top: 50,
        left: 125,
        margin: 0,
        borderBottomLeftRadius: 40,
        borderTopLeftRadius: 40,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.60,
        shadowRadius: 15.98,
        elevation: 26,
    },
    textInputName: {
        backgroundColor: '#FFFF',
        width: 340,
        height: 50,
        borderRadius: 30,
        margin: 0,
        top: 130,
        left: 20,
        paddingLeft: 30,
        color: "#168EB4",
        fontSize: 20,
        shadowColor: "#000",
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 21,
    },
    textInputPassword: {
        backgroundColor: '#FFFF',
        width: 340,
        height: 50,
        borderRadius: 30,
        paddingLeft: 30,
        margin: 0,
        top: 170,
        left: 20,
        color: "#168EB4",
        // fontSize: 18,
        // shadowColor: "#000",
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 21,
    },
    TouchableSave: {
        backgroundColor: '#126F85',
        borderRadius: 30,
        height: 60,
        width: 300,
        top: 240,
        left: 40,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.60,
        shadowRadius: 15.93,
        elevation: 10,
    },
    btnView: {
        borderRadius: 30,
        height: 40,
        width: 350,
        justifyContent: "center"
    },
    TouchableText: {
        textAlign: "center",
        alignContent: "center",
        borderRadius: 30,
        top: 10,
        right: 30,
        color: 'white',
        fontSize: 18
    },
    imgUser: {
        width: 20,
        height: 20,
        right: 10,

    },
    imgKey: {
        width: 20,
        height: 20, 
        right:10
    },
    forgotPassword:{
        fontWeight:"bold",
        color:'gray',
        fontSize:16,
        top:130,
        left:220


    }

})