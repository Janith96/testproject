import React, { Component } from 'react';
import {
    AppRegistry, Text, TextInput, View, Image, ImageBackground, Button, Alert, StatusBar,
    TouchableOpacity, ScrollView, AsyncStorage, StyleSheet
} from 'react-native';
import styles from './LoginScreenStyles';
import { Item } from 'native-base';

export default class login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            Password: '',
            // visible1: true,
            // visible2: false,

        };

    }
    showAlert() {
        Alert.alert(this.state.userName + "  " + this.state.Password)
    }



    namevalidate(text) {
        const reg = /^[a-zA-Z ]*$/;
        if (reg.test(text) === false) {
            return false;
        } else {
            return true;
        }
    }


    validation() {
        if (this.namevalidate(this.state.userName) || this.state.namevalidate(this.state.Password.length > 5)) {
            this.props.navigation.navigate('CustomerScreen')


        }
        // else if (this.state.userName == " " && this.state.Password == " ") { }
        // if (this.state.userName == "janith" && this.state.Password == "1234" ) {
        //     this.props.navigation.navigate('CustomerScreen')
        // } 

        else {
            Alert.alert("username or passsword incorrect!")
        }
    };

    // showPassword(){
    //     this.setState({
    //         visible1:false,
    //         visible2:true,
    //         secureTextEntry:false
    //     })

    // }

    // invisiblePassword(){

    // }


    render() {
        return (

            <ImageBackground
                source={require('../../assets/25_30.jpg')}
                style={styles.imageBackground}>

                <StatusBar
                    barStyle='dark-content'
                    backgroundColor='#126F85' />

                <View style={styles.bodyView}>

                    <View style={{
                        position: 'absolute',
                        top: 25,
                        left: 35
                    }}>
                        <Text style={{
                            fontSize: 40,
                            fontWeight: 'bold',
                            color: '#168EB4',
                        }}>L</Text>

                        <Text style={{
                            fontSize: 20,
                            fontWeight: 'bold',
                            color: '#0C4056',
                        }}>O</Text>

                        <Text style={{
                            fontSize: 30,
                            fontWeight: 'bold',
                            color: '#168EB4',
                        }}>G</Text>

                        <Text style={{
                            fontSize: 35,
                            fontWeight: 'bold',
                            color: '#0C4056',
                        }}>I</Text>

                        <Text style={{
                            fontSize: 30,
                            fontWeight: 'bold',
                            color: '#168EB4',
                        }}>N</Text>
                    </View>
                    <View style={styles.bodyBoxView}>
                        {/* <ImageBackground style={{ width: '100%', overflow:"hidden", height: '100%' }} 
                    source={require('../assets/1.jpeg')} > */}
                        <Image style={{ width: 100, height: 100, }}
                            source={require('../../assets/user.png')} />
                        {/* {this.state.visible1 && */}
                        {/* // <TouchableOpacity onPress={()=>this.showPassword()}>
                                
                            {/* </TouchableOpacity> */}

                        {/* {this.state.visible2 &&
                            <TouchableOpacity  onPress={()=>this.invisiblePassword()}>
                                <Image style={{ width: 100, height: 100, }}
                                    source={require('../../assets/user.png')} />
                            </TouchableOpacity>
                        } */}

                    </View>
                    <Item style={styles.textInputName}>
                        <Image style={styles.imgUser} source={require('../../assets/userName.png')} />
                        <TextInput
                            placeholder='   Username'
                            placeholderTextColor='black'
                            // fontSize='20%'
                            onChangeText={text => this.setState({ userName: text })}
                            value={this.state.userName} />
                    </Item>
                    <Item style={styles.textInputPassword}>
                        <Image style={styles.imgKey} source={require('../../assets/key.png')} />
                        <TextInput
                            secureTextEntry={this.state.visible}
                            placeholder='   Userpassword'
                            placeholderTextColor='black'

                            onChangeText={text => this.setState({ Password: text })}
                            value={this.state.Password} />
                    </Item>
                    <TouchableOpacity style={styles.TouchableSave}
                        onPress={() => this.validation()}>
                        <View style={styles.btnView}>
                            <Text style={styles.TouchableText}>Login</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={styles.forgotPassword}>Forgot Password ?</Text>
                </View>
            </ImageBackground>
        );
    }
}

